import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-salesnavbar',
  templateUrl: './salesnavbar.component.html',
  styleUrls: ['./salesnavbar.component.css']
})
export class SalesnavbarComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  navToLogin(){
    this.router.navigate([''])
  }

  navToAccount(){
    this.router.navigate(['./Sales/account']);
  }

  navToProduct(){
    this.router.navigate(['./Sales/product'])
  }
}
