import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  logins(){
    this.router.navigate(['./Marketing'])
  }

  loginm(){
    this.router.navigate(['./Sales'])
  }

}
