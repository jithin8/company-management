import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MarketingComponent } from './marketing/marketing.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CampaignComponent } from './campaign/campaign.component';
import { LeadComponent } from './lead/lead.component';
import { SalesComponent } from './sales/sales.component';
import { ProductComponent } from './product/product.component';
import { AccountComponent } from './account/account.component';
import { SalesnavbarComponent } from './salesnavbar/salesnavbar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MarketingComponent,
    NavbarComponent,
    CampaignComponent,
    LeadComponent,
    SalesComponent,
    ProductComponent,
    AccountComponent,
    SalesnavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
