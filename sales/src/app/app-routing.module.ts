import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MarketingComponent } from './marketing/marketing.component';
import { CampaignComponent } from './campaign/campaign.component';
import { LeadComponent } from './lead/lead.component';
import { SalesComponent } from './sales/sales.component';
import { ProductComponent } from './product/product.component';
import { AccountComponent } from './account/account.component';


const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'Marketing', component: MarketingComponent,
    children: [
      { path: '', component: CampaignComponent },
      { path: 'campaign', component: CampaignComponent },
      { path: 'lead', component: LeadComponent }
    ]
  }, {
    path: 'Sales', component: SalesComponent,
    children: [
      { path: '', component: AccountComponent },
      { path: 'product', component: ProductComponent },
      { path: 'account', component: AccountComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
