package com.jitihn.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.jitihn.dto.CampaignDTO;
import com.jitihn.model.Campaign;
import com.jitihn.model.Product;
import com.jitihn.utils.Utils;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class CampaignDAO implements PanacheRepositoryBase<Product, Long> {

    @Inject
    AccountDAO accountDAO;

    @Inject
    EntityManager entityManager;

    public long delete(String pid) {
        return Campaign.delete("pid", pid);
    }

    public Campaign create(Campaign campaign) {
        Campaign.persist(campaign);
        return campaign;
    }

    public List<CampaignDTO> getAll(String status) {
        if (status.equalsIgnoreCase(Utils.CampaignStatus.APPROVED)) {
            return Campaign.list("campaign_status", Utils.CampaignStatus.APPROVED);
        } else if (status.equalsIgnoreCase(Utils.CampaignStatus.PROCESSING)) {
            return Campaign.list("campaign_status", Utils.CampaignStatus.PROCESSING);
        } else {
            return Campaign.listAll();
        }

    }

    public Campaign get(String pid) {
        return Campaign.find("pid", pid).firstResult();
    }

    public Campaign update(String pid, CampaignDTO campaignDTO) {
        Campaign campaign = Campaign.find("pid", pid).firstResult();
        if (campaign != null) {
            entityManager.lock(campaign, LockModeType.PESSIMISTIC_WRITE);
            campaign.setCampaign_status(campaignDTO.getCampaign_status());
            campaign.setName(campaignDTO.getName());
            campaign.setStartdate(Utils.getDateFromString(campaignDTO.getStartdate()));
            campaign.setEnddate(Utils.getDateFromString(campaignDTO.getEnddate()));
            campaign.setDescription(campaignDTO.getDescription());
            campaign.setAccount(accountDAO.getFromPid(campaignDTO.getLeads()));
        }
        return campaign;
    }

    public Campaign changeStatus(String pid, String status) {
        Campaign campaign = Campaign.find("pid", pid).firstResult();
        if (campaign != null) {
            entityManager.lock(campaign, LockModeType.PESSIMISTIC_WRITE);
            campaign.setCampaign_status(status);
        }
        return campaign;
    }
}