package com.jitihn.dao;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.jitihn.dto.ProductDTO;
import com.jitihn.model.Product;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class ProductDAO implements PanacheRepositoryBase<Product, Long> {

    @Inject
    EntityManager entityManager;

    public List<ProductDTO> get(boolean active) {
        if (active) {
            return Product.list("active", true);
        }
        return Product.list("order by name");
    }

    public Product get(String pid) {
        return Product.find("pid", pid).firstResult();
    }

    public Product update(String pid, ProductDTO productDTO) {
        Product product = Product.find("pid", pid).firstResult();
        if (product != null) {
            entityManager.lock(product, LockModeType.PESSIMISTIC_WRITE);
            product.setActive(productDTO.getActive());
            product.setDescription(productDTO.getDescription());
            product.setName(productDTO.getName());
        }

        return product;
    }

    public long delete(String pid) {
        return Product.delete("pid", pid);
    }

    public Product create(Product product) {
        Product.persist(product);
        return product;
    }
}