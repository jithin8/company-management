package com.jitihn.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

import com.jitihn.dto.AccountDTO;
import com.jitihn.model.Account;
import com.jitihn.utils.Utils;

import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class AccountDAO implements PanacheRepositoryBase<Account, Long> {

    @Inject
    EntityManager entityManager;

    public List<Account> getFromPid(List<String> accountPids) {
        List<Account> accounts = new ArrayList<>();
        for (String pids : accountPids) {
            Account a = get(pids);
            if (a != null) {
                accounts.add(a);
            }
        }
        System.out.println(accounts);
        return accounts;
    }

    public Account create(Account account) {
        Account.persist(account);
        return account;
    }

    public List<AccountDTO> get(boolean active, boolean status, boolean approved) {
        if (approved) {
            System.out.println("approved called");
            return Account.list("leadstatus", "approved");
        }
        if (active && status) {
            return Account.list("active=?1 and (leadstatus=?2 or leadstatus=?3)", active, "created", "processing");
        } else if (active) {
            if (status) {
                return Account.list("leadstatus=?1 or leadstatus=?2", "created", "processing");
            }
            return Account.list("active", true);
        } else {
            if (status) {
                return Account.list("leadstatus=?1 or leadstatus=?2", "created", "processing");
            }
            return Account.listAll();
        }
    }

    public Account get(String pid) {
        return Account.find("pid", pid).firstResult();
    }

    public void updateStatus(List<Account> accounts, String status) {
        for (Account account : accounts) {
            Account tempAccount = Account.find("pid", account.getPid()).firstResult();
            if (tempAccount != null) {
                entityManager.lock(tempAccount, LockModeType.PESSIMISTIC_WRITE);
                tempAccount.setLeadstatus(status);
            }
        }
    }

    public Account confirmAccount(String pid) {
        Account account = Account.find("pid", pid).firstResult();
        if (account != null) {
            entityManager.lock(account, LockModeType.PESSIMISTIC_WRITE);
            account.setLeadstatus(Utils.LeadStatus.APPROVED);
        }
        return account;
    }

    public Account update(String pid, AccountDTO accountDTO) {
        Account account = Account.find("pid", pid).firstResult();
        if (account != null) {
            entityManager.lock(account, LockModeType.PESSIMISTIC_WRITE);
            // account.setActive(accountDTO.getActive());

            if (accountDTO.getLeadstatus() != null && !accountDTO.getLeadstatus().isEmpty()) {
                account.setLeadstatus(accountDTO.getLeadstatus());
            }

            if (accountDTO.getName() != null && !accountDTO.getName().isEmpty()) {
                account.setName(accountDTO.getName());
            }
            if (accountDTO.getContact() != null && !accountDTO.getContact().isEmpty()) {
                account.setContact(accountDTO.getContact());
            }

            if (accountDTO.getOrganisation() != null && !accountDTO.getOrganisation().isEmpty()) {
                account.setOrganisation(accountDTO.getOrganisation());
            }

            if (accountDTO.getEmail() != null && !accountDTO.getEmail().isEmpty()) {
                account.setEmail(accountDTO.getEmail());
            }
        }
        return account;
    }

    public long delete(String pid) {

        return Account.delete("pid", pid);
    }

}