package com.jitihn.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.jitihn.dto.AccountDTO;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Cacheable
public class Account extends PanacheEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "accountSequence")
    public Long id;
    private String pid;
    private String name;
    private String organisation;
    private String email;
    private String contact;
    private String leadstatus;
    private Boolean active;

    @ManyToMany(mappedBy = "account", cascade = CascadeType.ALL)
    private List<Campaign> campaign = new ArrayList<>();

    public Account(AccountDTO dto) {
        this.pid = dto.getPid();
        this.name = dto.getName();
        this.organisation = dto.getOrganisation();
        this.email = dto.getEmail();
        this.contact = dto.getContact();
        this.leadstatus = dto.getLeadstatus().toString();
        this.active = dto.getActive();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLeadstatus() {
        return leadstatus;
    }

    public void setLeadstatus(String leadstatus) {
        this.leadstatus = leadstatus;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Account() {
    }

    @Override
    public String toString() {
        return "Account [active=" + active + ", campaign=" + campaign + ", contact=" + contact + ", email=" + email
                + ", id=" + id + ", leadstatus=" + leadstatus + ", name=" + name + ", organisation=" + organisation
                + ", pid=" + pid + "]";
    }
}