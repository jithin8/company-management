package com.jitihn.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.validation.ConstraintViolation;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {
    private static Logger logger = LoggerFactory.getLogger(Utils.class);

    public static JSONObject objectToJsonObject(Object object) {
        String jsonString = objectToJson(object);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (Exception err) {
            logger.info("Error", err.toString());
        }
        return jsonObject;
    }

    public static String generateUniqueId() {
        return UUID.randomUUID().toString();
    }

    public static String objectToJson(Object object) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.toJson(object);
    }

    public static Object jsonToObject(String json, Object object) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.fromJson(json, object.getClass());
    }

    public static Object createResponseMessage(Map<String, Object> jsonObj) {
        for (Map.Entry<String, Object> entry : jsonObj.entrySet()) {
            jsonObj.put(entry.getKey(), entry.getValue());
        }
        return objectToJsonObject(jsonObj);
    }

    public static String validationResult(Set<? extends ConstraintViolation<?>> violations) {
        return violations.stream().map(cv -> cv.getMessage()).collect(Collectors.joining(", "));
    }

    public static Response JsonResponse(Status status, String message, boolean isSuccess, Map<String, Object> jsonObj) {
        Object msgObj = null;
        if (jsonObj == null) {
            jsonObj = new HashMap<String, Object>();
        }
        jsonObj.put("success", isSuccess);
        jsonObj.put("message", message);
        msgObj = createResponseMessage(jsonObj);
        return Response.status(status).entity(msgObj.toString()).build();
    }

    public static String getFileName(MultivaluedMap<String, String> header) {

        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {

                String[] name = filename.split("=");

                String finalFileName = name[1].trim().replaceAll("\"", "");
                return finalFileName;
            }
        }
        return "unknown";
    }

    // save to somewhere
    // public static void writeFile(byte[] content, String filename) throws
    // IOException {
    // File file = new File(filename);
    // System.out.println("jithin file to write " + file.getAbsolutePath());
    // if (!file.exists()) {
    // file.createNewFile();
    // }
    // FileOutputStream fop = new FileOutputStream(file);
    // fop.write(content);
    // fop.flush();
    // fop.close();
    // }
    public static void writeFile(byte[] content, String filename) throws IOException {
        Path file = Paths.get(filename);
        Files.write(file, content);
    }

    public class LeadStatus {
        public static final String CREATED = "created";
        public static final String PROCESSING = "processing";
        public static final String APPROVED = "approved";
    }

    public class CampaignStatus {
        public static final String ALL = "all";
        public static final String PROCESSING = "processing";
        public static final String APPROVED = "approved";
    }

    public static String getStringFromDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatter);
    }

    // dd-MM-yyyy
    public static LocalDate getDateFromString(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}