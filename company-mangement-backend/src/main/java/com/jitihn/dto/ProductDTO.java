package com.jitihn.dto;

import com.jitihn.model.Product;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

public class ProductDTO extends PanacheEntity {
    private String pid;
    private String name;
    private String description;
    private Boolean active;

    public ProductDTO() {

    }

    public ProductDTO(Product product) {
        this.pid = product.getPid();
        this.name = product.getName();
        this.description = product.getDescription();
        this.active = product.getActive();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

}