package com.jitihn.dto;

import com.jitihn.model.Account;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

public class AccountDTO extends PanacheEntity {
    private String pid;
    private String name;
    private String organisation;
    private String email;
    private String contact;
    private String leadstatus;
    private Boolean active;

    public AccountDTO(Account account) {
        this.pid = account.getPid();
        this.name = account.getName();
        this.organisation = account.getOrganisation();
        this.email = account.getEmail();
        this.contact = account.getContact();
        this.leadstatus = account.getLeadstatus();
        this.active = account.getActive();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public AccountDTO() {
    }

    public String getLeadstatus() {
        return leadstatus;
    }

    public void setLeadstatus(String leadstatus) {
        this.leadstatus = leadstatus;
    }

    @Override
    public String toString() {
        return "AccountDTO [active=" + active + ", contact=" + contact + ", email=" + email + ", leadstatus="
                + leadstatus + ", name=" + name + ", organisation=" + organisation + ", pid=" + pid + "]";
    }

}