package com.jitihn.dto;

import java.util.List;
import com.jitihn.model.Campaign;
import com.jitihn.utils.Utils;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

public class CampaignDTO extends PanacheEntity {
    private String pid;
    private String name;
    private String description;
    private String startdate;
    private String enddate;
    public List<String> leads;
    private String campaign_status;

    public CampaignDTO(Campaign campaign) {
        this.pid = campaign.getPid();
        this.name = campaign.getName();
        this.description = campaign.getDescription();
        this.startdate = Utils.getStringFromDate(campaign.getStartdate());
        this.enddate = Utils.getStringFromDate(campaign.getEnddate());
        this.campaign_status = campaign.getCampaign_status();
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CampaignDTO() {
    }

    public List<String> getLeads() {
        return leads;
    }

    public void setLeads(List<String> leads) {
        this.leads = leads;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getCampaign_status() {
        return campaign_status;
    }

    public void setCampaign_status(String campaign_status) {
        this.campaign_status = campaign_status;
    }

}