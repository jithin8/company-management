package com.jitihn.resource;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dao.AccountDAO;
import com.jitihn.dto.AccountDTO;
import com.jitihn.model.Account;
import com.jitihn.utils.Utils;

@Path("/api/lead")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LeadResource {

    @Inject
    AccountDAO accountDAO;

    @POST
    @Transactional
    public Response create(AccountDTO accountDTO) {
        accountDTO.setLeadstatus(Utils.LeadStatus.CREATED);
        accountDTO.setActive(true);
        accountDTO.setPid(Utils.generateUniqueId());
        Account a = new Account(accountDTO);
        Account account = accountDAO.create(a);
        if (account != null) {
            accountDTO = new AccountDTO(account);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", accountDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "created", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @GET
    public Response get(@DefaultValue("false") @QueryParam("active") boolean active,
            @DefaultValue("false") @QueryParam("status") boolean status, @QueryParam("approved") boolean approved) {
        System.out.println("dsa" + active + "status" + status);
        List<AccountDTO> accountDTO = accountDAO.get(active, status, approved);
        if (accountDTO != null && accountDTO.size() > 0) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", accountDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "listed", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @GET
    @Path("/{pid}")
    public Response getByPid(@PathParam("pid") String pid) {
        Account account = accountDAO.get(pid);
        if (account != null) {
            AccountDTO dto = new AccountDTO(account);
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", dto);
            return Utils.JsonResponse(Status.ACCEPTED, "get", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @DELETE
    @Path("/{pid}")
    @Transactional
    public Response delete(@PathParam("pid") String pid) {
        long productID = accountDAO.delete(pid);
        // HashMap<String, Object> obj = new HashMap<>();
        // obj.put("data", productID);
        if (productID == 1) {
            return Utils.JsonResponse(Status.ACCEPTED, "deleted", true, null);
        }
        return Utils.JsonResponse(Status.ACCEPTED, "cannot deleted", false, null);
    }

    @PATCH
    @Transactional
    @Path("/{pid}")
    public Response update(@PathParam("pid") String pid, AccountDTO accountDTO) {
        Account a = accountDAO.update(pid, accountDTO);
        if (a != null) {
            accountDTO = new AccountDTO(a);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", accountDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "updated", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @PUT
    @Transactional
    @Path("/{pid}/confirm")
    public Response updateStatus(@PathParam("pid") String pid) {
        Account a = accountDAO.confirmAccount(pid);
        if (a != null) {
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", new AccountDTO(a));
            return Utils.JsonResponse(Status.ACCEPTED, "changed status", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

}