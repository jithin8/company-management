package com.jitihn.resource;

import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import com.jitihn.dao.ProductDAO;
import com.jitihn.dto.ProductDTO;
import com.jitihn.model.Product;
import com.jitihn.utils.Utils;

@Path("/api/product")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {

    @Inject
    ProductDAO productDAO;

    // @Override
    @GET
    public Response get(@DefaultValue("false") @QueryParam("active") boolean active) {
        List<ProductDTO> product = productDAO.get(active);
        if (product != null && product.size() > 0) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", product);
            return Utils.JsonResponse(Status.ACCEPTED, "listed", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @GET
    @Path("/{pid}")
    public Response getByPid(@PathParam("pid") String pid) {
        Product product = productDAO.get(pid);
        if (product != null) {
            ProductDTO dto = new ProductDTO(product);
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", dto);
            return Utils.JsonResponse(Status.ACCEPTED, "get", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @DELETE
    @Path("/{pid}")
    @Transactional
    public Response delete(@PathParam("pid") String pid) {
        long productID = productDAO.delete(pid);
        // HashMap<String, Object> obj = new HashMap<>();
        // obj.put("data", productID);
        if (productID == 1) {
            return Utils.JsonResponse(Status.ACCEPTED, "deleted", true, null);
        }
        return Utils.JsonResponse(Status.ACCEPTED, "cannot deleted", false, null);
    }

    @PATCH
    @Transactional
    @Path("/{pid}")
    public Response update(@PathParam("pid") String pid, ProductDTO productDTO) {
        Product p = productDAO.update(pid, productDTO);
        if (p != null) {
            productDTO = new ProductDTO(p);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", productDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "updated", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @POST
    @Transactional
    public Response create(ProductDTO productDTO) {
        productDTO.setActive(true);
        productDTO.setPid(Utils.generateUniqueId());
        Product p = new Product(productDTO);
        Product product = productDAO.create(p);
        if (product != null) {
            productDTO = new ProductDTO(product);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", productDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "created", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

}