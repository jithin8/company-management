package com.jitihn.resource;

import java.util.HashMap;
import java.util.List;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dao.AccountDAO;
import com.jitihn.dao.CampaignDAO;
import com.jitihn.dto.CampaignDTO;
import com.jitihn.model.Campaign;
import com.jitihn.utils.Utils;

@Path("/api/campaign")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CampaignResource {

    @Inject
    CampaignDAO campaignDAO;

    @Inject
    AccountDAO accountDAO;

    @POST
    @Transactional
    public Response create(CampaignDTO campaignDTO) {
        campaignDTO.setCampaign_status(Utils.CampaignStatus.PROCESSING);
        campaignDTO.setPid(Utils.generateUniqueId());
        Campaign c = new Campaign(campaignDTO);
        Campaign campaign = campaignDAO.create(c);
        if (campaign != null) {

            // change when to approved
            // List<Account> processedAccount = campaign.getAccount();
            // accountDAO.updateStatus(processedAccount, Utils.LeadStatus.PROCESSING);
            campaignDTO = new CampaignDTO(campaign);
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", campaignDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "created", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @GET
    public Response get(@DefaultValue("all") @QueryParam("status") String status) {
        List<CampaignDTO> campaignDTO = campaignDAO.getAll(status);
        if (campaignDTO != null && campaignDTO.size() > 0) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", campaignDTO);
            return Utils.JsonResponse(Status.ACCEPTED, "listed", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @GET
    @Path("/{pid}")
    public Response getByPid(@PathParam("pid") String pid) {
        Campaign campaign = campaignDAO.get(pid);
        if (campaign != null) {
            // success
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", campaign);
            return Utils.JsonResponse(Status.ACCEPTED, "get", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @DELETE
    @Path("/{pid}")
    @Transactional
    public Response delete(@PathParam("pid") String pid) {
        long productID = campaignDAO.delete(pid);
        // HashMap<String, Object> obj = new HashMap<>();
        // obj.put("data", productID);
        if (productID == 1) {
            return Utils.JsonResponse(Status.ACCEPTED, "deleted", true, null);
        }
        return Utils.JsonResponse(Status.ACCEPTED, "cannot deleted", false, null);
    }

    @PATCH
    @Transactional
    @Path("/{pid}")
    public Response update(@PathParam("pid") String pid, CampaignDTO campaignDTO) {
        Campaign c = campaignDAO.update(pid, campaignDTO);
        if (c != null) {
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", c);
            return Utils.JsonResponse(Status.ACCEPTED, "updated", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

    @PUT
    @Transactional
    @Path("/{pid}/status/{status}")
    public Response update(@PathParam("pid") String pid, @PathParam("status") String status) {
        Campaign c = campaignDAO.changeStatus(pid, status);
        if (c != null) {
            HashMap<String, Object> obj = new HashMap<>();
            obj.put("data", c);
            return Utils.JsonResponse(Status.ACCEPTED, "updated", true, obj);
        } else {
            return Utils.JsonResponse(Status.ACCEPTED, "not found", false, null);
        }
    }

}