import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: 'http://localhost:8181/auth',
  realm: 'company-management',
  clientId: 'frontend'
};

export const environment = {
  production: false,
  keycloakConfig
};
