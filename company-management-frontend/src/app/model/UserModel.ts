export class UserData {
    id: string;
    firstName: string;
    lastName: string;
    username: string;
    enabled: boolean;
    email: string;
    groups: string[];
    credentials: PasswordData[];
    attributes: Attributes
}

export class LeadData {
    name: string
    organisation: string
    contact: string
    email: string
    leadstatus: string
    status: boolean
}

export class ProductData {
    name: string
    description: string
}

export class LeadStatus {
    static readonly CREATED: string = "created"
    static readonly PROCESSING: string = "processing"
    static readonly APPROVED: string = "approved"
}

export class CampaignData {
    name: string
    description: string
    startdate: string
    enddate: string
    leads: []
}


export class Attributes {
    avatar_url: string;
    mobile_number: string;
}

export class PasswordData {
    type: string;
    value: string;
    temporary: boolean;
}
