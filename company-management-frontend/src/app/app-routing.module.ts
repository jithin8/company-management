import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LeadComponent } from './components/lead/lead.component';
import { CampaignComponent } from './components/campaign/campaign.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { ProductComponent } from './components/product/product.component';
import { TestComponent } from './components/test/test.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'v1',
    pathMatch: 'full'
  },
  {
    path: 'v1',
    component: HomeComponent,
    children: [
      {
        path: 'lead',
        component: LeadComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'campaign',
        component: CampaignComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'accounts',
        component: AccountsComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'product',
        component: ProductComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'test',
        component: TestComponent,
        canActivate: [AuthGuard],
      },
    ]
  },

  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
