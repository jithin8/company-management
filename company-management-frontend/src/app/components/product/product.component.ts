import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/service/toast.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductData } from 'src/app/model/UserModel';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private dataService: DataService, private modalService: NgbModal, public toastService: ToastService) { }

  ngOnInit() {
    this.getProducts()
  }
  selectedProductPid = ""
  productData = []
  msg: string = ""
  status: boolean

  createProductForm = new FormGroup({
    productName: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
  });

  getProducts() {
    this.dataService.getProduct().subscribe(res => {
      console.log(res);

      if (res["success"]) {
        this.productData = res["data"]
        console.log(this.productData);

      } else {
        this.productData = []
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }

  getProduct() {
    if (this.selectedProductPid != null && this.selectedProductPid.length > 0) {
      console.log("update now");

      this.dataService.getProductWithPid(this.selectedProductPid).subscribe(res => {
        if (res["success"]) {
          let d = res["data"]
          console.log(d);
          this.createProductForm.get("productName").setValue(d["name"])
          this.createProductForm.get("description").setValue(d["description"])

          // this.leadStatus = d["leadstatus"]
          // this.createLeadForm.get("leadName").setValue(d["name"])
          // this.createLeadForm.get("organisation").setValue(d["organisation"])
          // this.createLeadForm.get("contact").setValue(d["contact"])
          // this.createLeadForm.get("email").setValue(d["email"])


        } else {
          console.log("cannot get product");
        }
      }, err => {

        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }

  createProduct() {
    let productData: ProductData = new ProductData();
    productData.name = this.createProductForm.get("productName").value;
    productData.description = this.createProductForm.get("description").value;

    console.log("create product");
    console.log(productData);
    this.dataService.createProduct(productData).subscribe(res => {
      console.log();
      if (res["success"]) {
        this.createProductForm.reset();
        this.msg = res["message"]
        this.status = true
        setTimeout(() => {
          this.modalService.dismissAll()
          this.msg = ""
          this.getProducts()
        },
          1000);
      }

    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }

  deleteProduct() {
    if (this.selectedProductPid != null && this.selectedProductPid.length > 0) {
      console.log("delete now");

      this.dataService.deleteProduct(this.selectedProductPid).subscribe(res => {
        if (res["success"]) {
          this.modalService.dismissAll()
          this.getProducts()
        } else {
          console.log("cannot delete");
        }
      }, err => {

        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }

  updateProduct() {
    let productData: ProductData = new ProductData();
    productData.name = this.createProductForm.get("productName").value;
    productData.description = this.createProductForm.get("description").value;

    console.log("update lead");
    console.log(productData);
    this.dataService.updateProduct(this.selectedProductPid, productData).subscribe(res => {
      console.log();
      if (res["success"]) {
        this.createProductForm.reset();
        this.msg = res["message"]
        this.status = true
        setTimeout(() => {
          this.modalService.dismissAll()
          this.msg = ""
          this.getProducts()
        },
          1000);
      }

    }, err => {

      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });

    })
  }

  open(content, pid?: string, operation?: string) {
    this.selectedProductPid = pid
    console.log(pid);
    console.log(content);

    if (operation !== "delete") {
      this.getProduct()
    }

    this.modalService.open(content)
  }

}
