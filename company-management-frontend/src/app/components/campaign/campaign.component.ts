import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { CampaignData } from 'src/app/model/UserModel';
import { ToastService } from 'src/app/service/toast.service';

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {
  msg: any;
  status: boolean;
  ngOnInit(): void {
    this.getCampaigns()
  }

  leadData = [];
  campaignData = [];
  constructor(private modalService: NgbModal, private dataService: DataService, private toastService: ToastService) {
    this.addCheckboxes()
  }

  private addCheckboxes() {
    this.dataService.getLeadActive().subscribe(res => {
      if (res["success"]) {
        var leads = res["data"]
        this.createLeadData(leads)
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }
  createLeadData(leads: any) {
    leads.forEach(e => {
      this.leadData.push({
        pid: e["pid"],
        name: e["name"]
      })
    });
    console.log(this.leadData);

    this.leadData.forEach((o, i) => {
      const control = new FormControl(i === 0); // if first item set to true, else false
      (this.createCampaignForm.get('leadSelected') as FormArray).push(control);
    });

  }

  createCampaignForm = new FormGroup({
    campaignName: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    leadSelected: new FormArray([]),
  });
  accountsSelected = []
  getCampaign() {
    if (this.selectedCampaignPid != null && this.selectedCampaignPid.length > 0) {
      console.log("update now");

      this.dataService.getCampaignWithPid(this.selectedCampaignPid).subscribe(res => {
        if (res["success"]) {
          let d = res["data"]
          console.log(d)
          // this.leadStatus = d["leadstatus"]
          this.createCampaignForm.get("campaignName").setValue(d["name"])
          this.createCampaignForm.get("description").setValue(d["description"])
          this.createCampaignForm.get("startDate").setValue(d["startdate"])
          this.createCampaignForm.get("endDate").setValue(d["enddate"])
          this.campaignStatus = d["campaign_status"]
          this.accountsSelected = d["account"]
          // this.createCampaignForm.get("organisation").setValue(d["organisation"])
          // this.createCampaignForm.get("contact").setValue(d["contact"])
          // this.createCampaignForm.get("email").setValue(d["email"])


        } else {
          console.log("cannot get lead");
        }
      }, err => {
        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }


  selectedCampaignPid = ''
  open(content, pid?: string, operation?: string) {
    this.selectedCampaignPid = pid

    if (operation !== "delete") {
      this.getCampaign()
    }
    this.modalService.open(content)
  }

  createCampaign() {
    let campaignData: CampaignData = new CampaignData();
    campaignData.name = this.createCampaignForm.get("campaignName").value;
    campaignData.description = this.createCampaignForm.get("description").value;
    campaignData.startdate = this.createCampaignForm.get("startDate").value;
    campaignData.enddate = this.createCampaignForm.get("endDate").value;
    const selectedOrderIds = this.createCampaignForm.get('leadSelected').value
      .map((v, i) => v ? this.leadData[i].pid : null)
      .filter(v => v !== null);
    campaignData.leads = selectedOrderIds

    this.dataService.createCampaign(campaignData).subscribe(res => {
      console.log(res);
      this.createCampaignForm.reset();
      this.msg = res["message"]
      this.status = true
      setTimeout(() => {
        this.modalService.dismissAll()
        this.msg = ""
        this.getCampaigns()
      },
        1000);
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }

  getCampaigns() {
    this.dataService.getCampaign().subscribe(res => {
      if (res["success"]) {
        this.campaignData = res["data"]
      } else {
        this.campaignData = []
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }

  deleteCampaign() {
    if (this.selectedCampaignPid != null && this.selectedCampaignPid.length > 0) {

      this.dataService.deleteCampaign(this.selectedCampaignPid).subscribe(res => {
        if (res["success"]) {
          this.modalService.dismissAll()
          this.getCampaigns()
        } else {
          console.log("cannot delete");
        }
      }, err => {
        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }

  campaignStatus = ''

  approveCampaign() {
    if (this.selectedCampaignPid != null && this.selectedCampaignPid.length > 0) {

      this.dataService.confirmCampaign(this.selectedCampaignPid, "approved").subscribe(res => {
        if (res["success"]) {
          this.modalService.dismissAll()
          this.getCampaigns()
        } else {
          console.log("cannot delete");
        }
      }, err => {
        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }

}
