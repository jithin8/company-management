import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { ToastService } from 'src/app/service/toast.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  leadData;
  constructor(private dataService: DataService, private toastService: ToastService) { }


  ngOnInit() {
    this.getAccounts()
  }


  getAccounts() {
    this.dataService.getAccount().subscribe(res => {
      console.log(res);

      if (res["success"]) {
        this.leadData = res["data"]
        console.log(this.leadData);

      } else {
        this.leadData = []
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }
}
