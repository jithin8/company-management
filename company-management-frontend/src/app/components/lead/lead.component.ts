import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LeadData, LeadStatus } from 'src/app/model/UserModel';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastService } from 'src/app/service/toast.service';

@Component({
  selector: 'app-lead',
  templateUrl: './lead.component.html',
  styleUrls: ['./lead.component.css']
})
export class LeadComponent implements OnInit {
  leadData;
  constructor(private dataService: DataService, private modalService: NgbModal, public toastService: ToastService) { }

  getLeads() {
    this.dataService.getLead().subscribe(res => {
      console.log(res);

      if (res["success"]) {
        this.leadData = res["data"]
        console.log(this.leadData);

      } else {
        this.leadData = []
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }
  ngOnInit() {
    this.getLeads()
  }

  msg: string = ""
  status: boolean = true

  createLeadForm = new FormGroup({
    leadName: new FormControl('', [Validators.required]),
    organisation: new FormControl('', [Validators.required]),
    contact: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required, Validators.email])
  });

  createLead() {

    let leadData: LeadData = new LeadData();
    leadData.name = this.createLeadForm.get("leadName").value;
    leadData.organisation = this.createLeadForm.get("organisation").value;
    leadData.contact = this.createLeadForm.get("contact").value;
    leadData.email = this.createLeadForm.get("email").value;

    console.log("create lead");
    console.log(leadData);
    this.dataService.createLead(leadData).subscribe(res => {
      console.log();
      if (res["success"]) {
        this.createLeadForm.reset();
        this.msg = res["message"]
        this.status = true
        setTimeout(() => {
          this.modalService.dismissAll()
          this.msg = ""
          this.getLeads()
        },
          1000);
      }

    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })

  }

  selectedLeadPid = ''

  deleteLead() {
    if (this.selectedLeadPid != null && this.selectedLeadPid.length > 0) {
      console.log("delete now");

      this.dataService.deleteLead(this.selectedLeadPid).subscribe(res => {
        if (res["success"]) {
          this.modalService.dismissAll()
          this.getLeads()
        } else {
          console.log("cannot delete");
        }
      }, err => {

        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }
  leadStatus: string = ''
  getLead() {
    if (this.selectedLeadPid != null && this.selectedLeadPid.length > 0) {
      console.log("update now");

      this.dataService.getLeadWithPid(this.selectedLeadPid).subscribe(res => {
        if (res["success"]) {
          let d = res["data"]
          this.leadStatus = d["leadstatus"]
          this.createLeadForm.get("leadName").setValue(d["name"])
          this.createLeadForm.get("organisation").setValue(d["organisation"])
          this.createLeadForm.get("contact").setValue(d["contact"])
          this.createLeadForm.get("email").setValue(d["email"])


        } else {
          console.log("cannot get lead");
        }
      }, err => {

        console.log(err);
        this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
      })
    }
  }

  updateLead() {
    let leadData: LeadData = new LeadData();
    leadData.name = this.createLeadForm.get("leadName").value;
    leadData.organisation = this.createLeadForm.get("organisation").value;
    leadData.contact = this.createLeadForm.get("contact").value;
    leadData.email = this.createLeadForm.get("email").value;

    console.log("update lead");
    console.log(leadData);
    this.dataService.updateLead(this.selectedLeadPid, leadData).subscribe(res => {
      console.log();
      if (res["success"]) {
        this.createLeadForm.reset();
        this.msg = res["message"]
        this.status = true
        setTimeout(() => {
          this.modalService.dismissAll()
          this.msg = ""
          this.getLeads()
        },
          1000);
      }

    }, err => {

      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });

    })
  }




  open(content, pid?: string, operation?: string) {
    this.selectedLeadPid = pid
    console.log(pid);
    console.log(content);

    if (operation === "update") {
      this.getLead()
    }
    if (operation === "view") {
      this.getLead()
    }

    this.modalService.open(content)


    // this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
    //   // this.closeResult = `Closed with: ${result}`;
    // }, (reason) => {
    //   // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    // });
  }

  convertToAccount() {
    this.dataService.confirmAccount(this.selectedLeadPid).subscribe(res => {
      console.log();
      if (res["success"]) {
        this.getLeads()
        this.modalService.dismissAll()
      }
    }, err => {
      console.log(err);
      this.toastService.show(err['status'] + " " + err['statusText'], { classname: 'bg-danger text-light', delay: 5000 });
    })
  }

}
