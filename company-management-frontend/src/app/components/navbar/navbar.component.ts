import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { DataService } from 'src/app/service/data.service';
import { MykeycloakService } from 'src/app/service/mykeycloak.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  userDetails: any;
  username: string;
  constructor(private keycloakService: KeycloakService, private dataService: DataService, private myKeycloak: MykeycloakService) { }

  async ngOnInit() {
    this.getuserinfo()
    console.log("check here");

    console.log(this.keycloakService.getToken());

    console.log(this.myKeycloak.checkRoleInToken("sales-officer"));


  }

  async getuserinfo() {
    if (await this.keycloakService.isLoggedIn()) {
      this.username = this.keycloakService.getUsername()
    }
  }

  async doLogout() {
    await this.keycloakService.logout();
  }


}
