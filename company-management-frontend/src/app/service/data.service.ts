import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpParams, HttpClient } from '@angular/common/http';
import { UserData, LeadData, CampaignData, ProductData } from '../model/UserModel';
import { CompanyData } from '../model/CompanyModel';

@Injectable({
  providedIn: 'root'
})
export class DataService {





  baseUrl: string = "http://localhost:8181/auth/admin/realms/keycloak-permission/";
  keycloakLink: string = this.baseUrl + "users/";

  backendUrl: string = "http://localhost:8080/api/"

  constructor(private http: HttpClient) { }

  createLead(leadData: LeadData): Observable<LeadData> {
    console.log("Lead data");
    console.log(leadData);
    let link: string = this.backendUrl + "lead"
    return this.http.post<LeadData>(link, leadData)
  }

  createProduct(productData: ProductData): Observable<LeadData> {
    let link: string = this.backendUrl + "product"
    return this.http.post<LeadData>(link, productData)
  }

  getProduct() {
    let link: string = this.backendUrl + "product"
    return this.http.get(link);
  }

  updateLead(pid: string, leadData: LeadData): Observable<LeadData> {

    let link: string = this.backendUrl + "lead/" + pid
    return this.http.patch<LeadData>(link, leadData)
  }
  updateProduct(pid: string, productData: ProductData): Observable<ProductData> {

    let link: string = this.backendUrl + "product/" + pid
    return this.http.patch<ProductData>(link, productData)
  }
  confirmAccount(pid: string): Observable<LeadData> {
    let link: string = this.backendUrl + "lead/" + pid + "/confirm"
    return this.http.put<LeadData>(link, null)
  }
  getLead() {
    let link: string = this.backendUrl + "lead"
    return this.http.get(link);
  }
  getLeadWithPid(pid: string) {
    let link: string = this.backendUrl + "lead/" + pid
    return this.http.get(link);
  }
  getProductWithPid(pid: string) {
    let link: string = this.backendUrl + "product/" + pid
    return this.http.get(link);
  }

  deleteLead(pid: string) {
    let link: string = this.backendUrl + "lead/" + pid
    return this.http.delete(link);
  }
  deleteProduct(pid: string) {
    let link: string = this.backendUrl + "product/" + pid
    return this.http.delete(link);
  }
  deleteCampaign(pid: string) {
    let link: string = this.backendUrl + "campaign/" + pid
    return this.http.delete(link);
  }

  confirmCampaign(pid: string, status: string): Observable<CampaignData> {
    let link: string = this.backendUrl + "campaign/" + pid + "/status/" + status
    return this.http.put<CampaignData>(link, null)
  }

  getLeadActive() {
    let link: string = this.backendUrl + "lead"

    let params = new HttpParams();
    params = params.append('active', "true");
    params = params.append('status', "true");
    return this.http.get(link, { params: params });
  }

  getAccount() {
    let link: string = this.backendUrl + "lead"

    let params = new HttpParams();
    params = params.append('approved', "true");
    return this.http.get(link, { params: params });
  }

  createCampaign(campaignData: CampaignData) {
    let link: string = this.backendUrl + "campaign"
    return this.http.post<CampaignData>(link, campaignData)
  }

  getCampaign() {
    let link: string = this.backendUrl + "campaign"
    return this.http.get(link);
  }

  getCampaignWithPid(pid: string) {
    let link: string = this.backendUrl + "campaign/" + pid
    return this.http.get(link);
  }

  getUserByUsername(username: string): Observable<UserData> {
    let params = new HttpParams();
    params = params.append('username', username);
    return this.http.get<UserData>(this.keycloakLink, { params: params });
  }

  createCompany(companyData: CompanyData) {
    let link: string = this.backendUrl + "company"
    return this.http.post(link, companyData)
  }

  getCompanies() {
    let link: string = this.backendUrl + "company"
    return this.http.get(link)
  }


  createUser(userData: UserData): Observable<UserData> {
    console.log("create user");
    console.log(userData);

    let link: string = this.backendUrl + "keycloak/users"
    return this.http.post<UserData>(link, userData)
  }

}
