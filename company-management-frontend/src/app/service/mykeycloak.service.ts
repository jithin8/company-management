import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
@Injectable({
  providedIn: 'root'
})
export class MykeycloakService {
  constructor(private keycloakService: KeycloakService) {
  }
  getCurrentRoles() {
    return this.keycloakService.getKeycloakInstance().tokenParsed["realm_access"].roles
  }

  checkRoleInToken(checkRole: string) {
    var roleList = this.getCurrentRoles()
    for (let role of roleList) {
      if (checkRole === role) {
        return true
      }
    }
    return false
  }
}
